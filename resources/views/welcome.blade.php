<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Inicio</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
</head>
<body>
    <!-- Navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light">
        <div class="container-xxl container-fluid ">
            <a id="navbar-brand" class="navbar-brand" href="#">KB</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blog</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a class="btn primary-button" href="#">Contact Us</a>
                </span>
            </div>
        </div>
    </nav>

    <section id="header">
        <div class="header-content">
            <div class="container-fluid container-xl hero">
                <div class="row">
                    <div class="col-sm-12 col-md-5 header-content-container">
                        <div>
                            <h1 class="text--white text-heading">Build a business with a stunning <span class="text--green">knowledge base</span>. What can we help you <span class="text--green">find</span>?</h1>
                            <p class="header-content-text">KB is a fully featured knowledge base theme for any products and services.</p>
                            <!-- <div class="search-container">
                                <form action="#">
                                    <input class="form-control" placeholder="Search for something">
                                    <button class="btn secondary-button" type="submit"><i
                                        class="fas fa-search"></i></button>
                                </form>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7 svg-header-container">
                        <!-- <img src="{{ asset('img/vector-crop.svg') }}"> -->
                    </div>
                </div>
            </div>
        </div>
        <svg class="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path fill="#ffffff" fill-opacity="1"
                d="M0,32L34.3,48C68.6,64,137,96,206,96C274.3,96,343,64,411,48C480,32,549,32,617,80C685.7,128,754,224,823,234.7C891.4,245,960,171,1029,160C1097.1,149,1166,203,1234,224C1302.9,245,1371,235,1406,229.3L1440,224L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z">
            </path>
        </svg>
    </section>
    <section id="about" class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 col-md-10 col-sm-12 offset-xl-1 left">
                    <div class="sticky-element">
                        <h3 class="text-heading text--violet">WHY CHOOSE US?</h3>
                        <h2 class="text-heading">Check out why we’re the best company for all of your
                        needs.
                        </h2>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 right">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/shield.png" alt="Icon">
                                <h4 class="text-heading">Multi-factor authentication.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/flag.png" alt="Icon">
                                <h4 class="text-heading">Increase user conversion.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/heart-shape-outline-with-lifeline.png" alt="Icon">
                                <h4 class="text-heading">Full control over UX.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/selective.png" alt="Icon">
                                <h4 class="text-heading">Expand user funnel.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/favorites.png" alt="Icon">
                                <h4 class="text-heading">Security compliant.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 col-flex">
                            <div class="about-card">
                                <img src="img/icons/fire.png" alt="Icon">
                                <h4 class="text-heading">Integration in minutes.</h4>
                                <p>Increase learner engagement & knowledge retention in higher education and corporate
                                    training.
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-sm-12 about-link-container">
                            <a class="btn primary-button" href="#">View more choices <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">
            <div class="py-6">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="h-subheader mb-3">Our products</div>
                                <ul class="list-unstyled list-separated">
                                    <li><a class="link-muted active" href="/">UI Kit</a></li>
                                    <li><a class="link-muted" href="/icons">4713 open source icons</a></li>
                                    <li><a class="link-muted" href="/emails">Email templates</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="h-subheader mb-3">Support</div>
                                <ul class="list-unstyled list-separated">
                                    <li><a class="link-muted" href="/docs">Documentation</a></li>
                                    <li><a class="link-muted" href="/support">Support</a></li>
                                    <li><a class="link-muted" href="/guides">Guides</a></li>
                                    <li><a href="https://status.tabler.io" class="link-muted" target="_blank"
                                            rel="noopener noreferrer">Status</a></li>
                                    <li><a class="link-muted" href="/license">License</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-4">
                                <div class="h-subheader mb-3">Tabler</div>
                                <ul class="list-unstyled list-separated">
                                    <li><a class="link-muted" href="/about">About</a></li>
                                    <li><a class="link-muted" href="/testimonials">Testimonials</a></li>
                                    <li><a class="link-muted" href="/docs/getting-started/faq">FAQ</a></li>
                                    <li><a href="https://github.com/tabler/tabler/releases" class="link-muted"
                                            target="_blank" rel="noopener noreferrer">Releases</a></li>
                                    <li><a href="https://github.com/tabler" class="link-muted" target="_blank"
                                            rel="noopener noreferrer">Github</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ml-auto">
                        <div class="logo logo-gray mb-4"></div>
                        <div class="text-muted">Tabler comes with tons of well-designed components and features. Start
                            your adventure with Tabler and make your dashboard great again. For free!</div>
                        <div class="mt-4 mt-lg-6">
                            <div class="row gx-3">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
