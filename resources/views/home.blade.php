<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>
    <link rel="stylesheet" href="{{ asset('css/tabler.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
</head>

<body class="layout-fluid">
    <script src="{{ asset('js/demo-theme.min.js') }}"></script>
    <div class="page">
        <!-- Navbar -->
        <header class="navbar navbar-expand-md d-print-none">
            <div class="container-xl">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu"
                    aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Header logo -->
                <div class="navbar-brand mx-auto mx-sm-0">
                    <img src="{{ asset('img/membrete.svg') }}" width="110" height="32" alt="SET"
                        class="navbar-brand-image">
                </div>

                <div class="navbar-nav flex-row order-md-last mx-auto mx-sm-0">
                    <div class="navbar-brand">
                        <img src="{{ asset('img/SIIE.png') }}" width="110" height="32" alt="SET"
                            class="navbar-brand-image">
                    </div>
                </div>
            </div>
        </header>
        <div class="navbar-expand-md">
            <div class="collapse navbar-collapse" id="navbar-menu">
                <div class="navbar navbar-light">
                    <div class="container-xl">
                        <!-- Menu -->
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="">
                                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24"
                                            height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <polyline points="5 12 3 12 12 3 21 12 19 12" />
                                            <path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" />
                                            <path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" />
                                        </svg>
                                    </span>
                                    <span class="nav-link-title">
                                        Inicio
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#navbar-help" data-bs-toggle="dropdown"
                                    data-bs-auto-close="off" role="button">
                                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24"
                                            height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                            <circle cx="9" cy="7" r="4"></circle>
                                            <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"></path>
                                            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                            <path d="M21 21v-2a4 4 0 0 0 -3 -3.85"></path>
                                        </svg>
                                    </span>
                                    <span class="nav-link-title">
                                        Dropdown
                                    </span>
                                </a>
                                <div class="dropdown-menu" data-bs-popper="static">
                                    <a class="dropdown-item" href="">Item</a>
                                    <a class="dropdown-item" href="">Item</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-wrapper">


            <main>
                <section id="overview" class="section designed-for-the-future mt-3 p-lg-5">
                    <div class="container">
                        <h2 class="section-title designed-for-the-future__title text-center">Designed for the future
                        </h2>
                        <div class="row d-flex flex-row-reverse">
                            <div class="col-md-6 d-flex justify-content-center">
                                <picture class="designed-for-the-future__img-container">
                                    <source media="(min-width: 992px)"
                                        srcset="./images/illustration-editor-desktop.svg">
                                    <source media="(min-width: 768px)" srcset="./images/illustration-editor-mobile.svg">

                                    <img class="img-fluid" src="{{ asset('img/illustration-editor-mobile.svg') }}" />
                                </picture>
                            </div>
                            <div class="col-md-6 text-center text-lg-start">
                                <h3 class="section-title">Introducing an extensible editor</h3>
                                <p class="section-text">Blogr features an exceedingly intuitive interface which lets
                                    you focus on one thing:
                                    creating content.
                                    The editor supports management of multiple blogs and allows easy manipulation of
                                    embeds such as images,
                                    videos, and Markdown. Extensibility with plugins and themes provide easy ways to add
                                    functionality or
                                    change the looks of a blog.</p>
                                <h3 class="section-title">Robust content management</h3>
                                <p class="section-text mbc">Flexible content management enables users to easily move
                                    through posts. Increase
                                    the usability of your blog
                                    by adding customized categories, sections, format, or flow. With this functionality,
                                    you’re in full
                                    control.</p>
                            </div>
                        </div>
                </section>

                <section class="section state-of-the-art-infrastructure mt-5">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="state-of-the-art-infrastructure__img-col col-md-6">
                                <img class="img-fluid" src="./images/illustration-phones.svg" alt="">
                            </div>
                            <div class="col-md-6 text-center text-lg-start">
                                <h2 class="section-title state-of-the-art-infrastructure__title">State of the Art
                                    Infrastructure</h2>
                                <p class="section-text state-of-the-art-infrastructure__text">With reliability and
                                    speed in mind, worldwide
                                    data centers provide the backbone for ultra-fast connectivity.
                                    This ensures your site will load instantly, no matter where your readers are,
                                    keeping your site
                                    competitive.</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="pricing" class="s-open-source mt-5 p-lg-5">
                    <div class="container">
                        <div class="row d-flex flex-row-reverse align-items-center">
                            <div class="s-open-source__img-container col-md-6 d-flex justify-content-center">
                                <img class="img-fluid" src="./images/illustration-laptop-desktop.svg" alt="">
                            </div>
                            <div class="col-md-6 text-center text-lg-start">
                                <h2 class="section-title">Free, open, simple</h2>
                                <p class="section-text">Blogr is a free and open source application backed by a large
                                    community of helpful
                                    developers. It supports
                                    features such as code syntax highlighting, RSS feeds, social media integration,
                                    third-party commenting
                                    tools,
                                    and works seamlessly with Google Analytics. The architecture is clean and is
                                    relatively easy to learn.</p>
                                <h2 class="section-title">Powerful tooling</h2>
                                <p class="section-text pb-5">Batteries included. We built a simple and straightforward
                                    CLI tool that makes
                                    customization and deployment a breeze, but
                                    capable of producing even the most complicated sites.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <footer class="footer">
                <div class="container">
                    <div class="py-6">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <div class="h-subheader mb-3">Our products</div>
                                        <ul class="list-unstyled list-separated">
                                            <li><a class="link-muted active" href="/">UI Kit</a></li>
                                            <li><a class="link-muted" href="/icons">4713 open source icons</a></li>
                                            <li><a class="link-muted" href="/emails">Email templates</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="h-subheader mb-3">Support</div>
                                        <ul class="list-unstyled list-separated">
                                            <li><a class="link-muted" href="/docs">Documentation</a></li>
                                            <li><a class="link-muted" href="/support">Support</a></li>
                                            <li><a class="link-muted" href="/guides">Guides</a></li>
                                            <li><a href="https://status.tabler.io" class="link-muted" target="_blank"
                                                    rel="noopener noreferrer">Status</a></li>
                                            <li><a class="link-muted" href="/license">License</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="h-subheader mb-3">Tabler</div>
                                        <ul class="list-unstyled list-separated">
                                            <li><a class="link-muted" href="/about">About</a></li>
                                            <li><a class="link-muted" href="/testimonials">Testimonials</a></li>
                                            <li><a class="link-muted" href="/docs/getting-started/faq">FAQ</a></li>
                                            <li><a href="https://github.com/tabler/tabler/releases" class="link-muted"
                                                    target="_blank" rel="noopener noreferrer">Releases</a></li>
                                            <li><a href="https://github.com/tabler" class="link-muted"
                                                    target="_blank" rel="noopener noreferrer">Github</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 ml-auto">
                                <div class="logo logo-gray mb-4"></div>
                                <div class="text-muted">Tabler comes with tons of well-designed components and
                                    features. Start
                                    your adventure with Tabler and make your dashboard great again. For free!</div>
                                <div class="mt-4 mt-lg-6">
                                    <div class="row gx-3">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <script src="{{ asset('js/tabler.min.js') }}" defer></script>
</body>

</html>
