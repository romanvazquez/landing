<section id="header">
    <div class="header-content">
    <div class="container-fluid container-xl hero">
        <div class="row">
            <div class="col-sm-12 col-md-5 svg-header-container ">
                <img src="img/svg/illustration2.svg" alt="Mobile SVG illustration">
            </div>
            <div class="col-sm-12 col-md-7 header-content-container">
                <div>
                <h1 class="text--white text-heading">Build a business with a stunning <span class="text--green">knowledge base</span>. What can we help you <span class="text--green">find</span>?</h1>
                <p class="header-content-text">KB is a fully featured knowledge base theme for any products and services.</p>
                <div class="search-container">
                    <form action="#">
                        <input class="form-control" placeholder="Search for something">
                        <button class="btn secondary-button" type="submit"><i
                            class="fas fa-search"></i></button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <svg class="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#ffffff" fill-opacity="1"
        d="M0,32L34.3,48C68.6,64,137,96,206,96C274.3,96,343,64,411,48C480,32,549,32,617,80C685.7,128,754,224,823,234.7C891.4,245,960,171,1029,160C1097.1,149,1166,203,1234,224C1302.9,245,1371,235,1406,229.3L1440,224L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z">
    </path>
    </svg>
</section>
